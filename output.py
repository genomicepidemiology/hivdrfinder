import os, re
from argparse import ArgumentParser


def printOut(*lst):
   ''' This function check if debug is set, and prints all strings given. '''
   printLog("./outputlog.out", True, False, *lst)

def printLog(logfile, print2screen, addLineFeed, *lst):
   ''' This function prints to the screen and logs to a file, all the strings
   given.
   # print2screen eg. True, *lst is a commaseparated list of strings
   '''
   webmode=True
   if print2screen:
      if addLineFeed:
         print '\n'.join(str(string) +'<br>' if webmode else str(string) for string in lst)
      else:
         print ''.join(str(string) for string in lst)
   if os.path.exists(logfile):
      with open(logfile, 'a') as f:
         f.write('\n'.join(str(string) for string in lst) +"\n")
   elif not print2screen and not addLineFeed: # Print to screen if there is no outputfile
      print '\n'.join(str(string) +'<br>' if webmode else str(string) for string in lst)


def known_mut_out(tmp):
   pos_line = {"A": 0, "C": 1, "G": 2, "T": 3}
   td_lightgrey="<td class='bglightgrey'>"
   td_red="<td class='bgred'>"
   td_green="<td class='bggreen'>"
   td_lightgreen="<td class='bglightgreen'>"
   td_yellow="<td class='bgyellow'>"
   td_lightblue="<td class='bglightblue'>"
   pos_ref=pos_line[tmp[3]]
   pos_mut=pos_line[tmp[-2][2]]
   string_out1="<tr>"+td_lightgrey+"%s%s%s</td>"%(str("</td>"+td_lightgrey).join(tmp[:3]),str("</td>"+td_yellow+str(tmp[3])),str("</td>"+td_lightgrey+str(tmp[4])))
   list_out2=[td_lightgrey]*6
   list_out2[pos_ref]=td_yellow
   list_out2[pos_mut]=td_green
   string_out2=''.join([str(a) + str(b) + str(c) for a,b,c in zip(list_out2,tmp[5:11],["</td>"]*6)])
   string_out3=td_lightgrey+"%s</td>"%(str("</td>"+td_lightgrey).join(tmp[11:]))
   printOut(string_out1+string_out2+string_out3+"</tr>")

def unknown_mut_out(tmp, min_ab):
   pos_line = {"A": 0, "C": 1, "G": 2, "T": 3, "N":4, "-":5}
   td_lightgrey="<td class='bglightgrey'>"
   td_red="<td class='bgred'>"
   td_green="<td class='bggreen'>"
   td_lightgreen="<td class='bglightgreen'>"
   td_yellow="<td class='bgyellow'>"
   td_lightblue="<td class='bglightblue'>"
   pos_ref=pos_line[tmp[3]]
   if tmp[-1]==tmp[-2]:
       backcolor=td_lightblue
   else:
       backcolor=td_lightgrey
   string_out1="<tr>"+backcolor+"%s%s%s</td>"%(str("</td>"+backcolor).join(tmp[:3]),str("</td>"+td_yellow+str(tmp[3])),str("</td>"+backcolor+str(tmp[4])))
   list_out2=[backcolor]*6
   string_out2=''.join([str(a) + str(b) + str(c) for a,b,c in zip(list_out2,tmp[5:11],["</td>"]*6)])
   for i in range(6):
       if float(tmp[5+i])/float(tmp[11])>0.04:
           list_out2[i]=td_green
   list_out2[pos_ref]=td_yellow
   string_out2=''.join([str(a) + str(b) + str(c) for a,b,c in zip(list_out2,tmp[5:11],["</td>"]*6)])
   string_out3=backcolor+"%s</td>"%(str("</td>"+backcolor).join(tmp[11:]))
   printOut(string_out1+string_out2+string_out3+"</tr>")

def vir_out3html(fp, min_ab,nuc_link=False, nuc_link_idx=-1, aln_start=11, css_classes='',twidth='85%', mlst_format=False, twoheaders = False, aln_len_index = 0, id_index=1, cov_index=2,template_index=[0,-1]):
   ''' Convert CGEs extended output file format to HTML

   ARGS:
      * aln_start sets the starting position of the alignments. A 20 chars header is
        asumed by default.
      * if nuc_link is True, the last entry in the table will be a link to
        www.ncbi.nlm.nih.gov/nuccore/
   '''
   if os.path.exists(fp):
      templates = {}
      with open(fp, 'r') as f:
         in_table = False
         in_ext = False
         in_note = False
         l = True
         header2 = ""
         while l:
            l = f.readline()
            if in_ext:
               if l.strip() == '': continue
               if l.startswith('#'):
                  # Entry split - new entry
                  prev_color = 'grey'
                  a_buff = []
                  q_buff = []
                  template = l[1:].strip()

                  printOut("<h3><i>%s</i></h3>"%template)
                  aln_len = templates[template]
                  cur_len = 0
               else:
                  a_line = l
                  m_line = f.readline()
                  q_line = f.readline()
                  allele_txt = a_line[:aln_start].strip().rstrip(':')
                  allele = a_line[aln_start:].rstrip()
                  a_length = len(allele)
                  matches = m_line[aln_start:].rstrip()
                  query_txt = q_line[:aln_start].strip().rstrip(':')
                  query = q_line[aln_start:].rstrip()
                  q_length = len(query)
                  # Add title
                  a_buff.append("<span>%s</span>"%(allele_txt.ljust(aln_start)))
                  q_buff.append("<span>%s</span>"%(query_txt.ljust(aln_start)))
                  # Parse alignment
                  min_length = a_length if a_length < q_length else q_length
                  max_length = a_length if a_length > q_length else q_length
                  matches = matches.ljust(max_length)
                  cur_len += max_length
                  if cur_len > aln_len:
                     min_length -= cur_len - aln_len

                  i = 0
                  in_match = False
                  if prev_color == 'green':
                     in_match = False

                  while i < min_length:
                     # Select span color
                     is_match = matches[i] == '|'
                     if is_match:
                        color = 'green'
                        prev_color = 'green'
                     elif prev_color == 'grey':
                        color = 'grey'
                        prev_color = 'red'
                     else:
                        color = 'red'
                     a_buff.append("<span class='bg%s'>"%color)
                     q_buff.append("<span class='bg%s'>"%color)
                     # Find span length
                     while i < min_length:
                        if matches[i] != '|':
                           if is_match and matches[i] != '|':
                              end = i
                              break
                        else:
                           if not is_match:
                              end = i
                              break
                        a_buff.append(allele[i])
                        q_buff.append(query[i])
                        i+=1

                     a_buff.append("</span>")
                     q_buff.append("</span>")
                     if color == 'grey':
                        cur_len -= i

                  if i < max_length:
                     # Handle trailing end
                     prev_color = 'grey'
                     color = 'grey'
                     a_buff.append("<span class='bg%s'>"%color)
                     q_buff.append("<span class='bg%s'>"%color)
                     while i < max_length:
                        a_buff.append(allele[i] if i < a_length else ' ')
                        q_buff.append(query[i] if i < q_length else ' ')
                        i+=1

                     a_buff.append("</span>")
                     q_buff.append("</span>")
                  else:
                     prev_color = color

                  printOut("%s\n%s\n"%(''.join(a_buff), ''.join(q_buff)))
                  a_buff = []
                  q_buff = []
                  # print(cur_len)
            elif in_table:
               if l.strip() == '': continue
               tmp = re.split('  +',l.strip())
               if upper_header:
                  header1 = l.rstrip()
                  header2 = l.rstrip()
                  upper_header = False
               elif len(tmp) > 1:
                  if nohead:
                     # tsize = len(tmp)
                     nohead = False
                     if header1 != "":
                        printOut("<tr><th colspan='{}'>{}</th></tr>".format(len(tmp), header1))
                     printOut("<tr><th>%s</th></tr>\n"%("</th><th>".join(tmp)))
                  else:
                     # Replace placeholders with empty fields

                     tmp = list(map(lambda x: '' if x == '-' else x, tmp))

                     if "No hit found" in tmp:
                        printOut("<tr class='bgred'><td align='center' colspan='%s'>%s</td></tr>\n"%(len(tmp), "".join(tmp)))
                        continue
                     # Select row color
                     try:
                        p_id = float(tmp[id_index]) == 100
                        if "/" not in tmp[cov_index]:
                           p_cov = float(tmp[cov_index]) == 100
                        else:
                           p_cov = float(tmp[cov_index].split("/")[0]) / float(tmp[cov_index].split("/")[1]) == 1
                     except ValueError:
                        p_id = False
                        p_cov = False
                     except IndexError:
                        p_id = False
                        p_cov = False
                     color = 'bgred'
                     if p_id and p_cov:
                        color = 'bggreen'
                     elif not p_cov:
                        color = 'bggrey'
                     elif not p_id:
                        color = 'bglightgreen'

                     # Store template lengths
                     template = []
                     for i in template_index:
                        template.append(tmp[i])
                     template = "_".join(template)
                     #template = tmp[0] + "_" + tmp[-1]

                     # Add link to match
                     if nuc_link:
                        try:
                           tmp[int(nuc_link_idx)] = "<a href='http://www.ncbi.nlm.nih.gov/nuccore/%s'>%s</a>"%(tmp[int(nuc_link_idx)], tmp[int(nuc_link_idx)])
                        except:
                           tmp[-1] = "<a href='http://www.ncbi.nlm.nih.gov/nuccore/%s'>%s</a>"%(tmp[-1], tmp[-1])
                     if header2 == "Genes found":
                        printOut("<tr class='%s'><td>%s</td></tr>\n"%(color,"</td><td>".join(tmp)))
                     elif header2 == "Known mutations":
                        known_mut_out(tmp)
                     elif header2 == "Unknown mutations":
                        unknown_mut_out(tmp,min_ab)
                     try:
                        if aln_len_index != 0:
                           aln_len = int(tmp[aln_len_index])
                        else:
                           aln_len = int(tmp[cov_index].split("/")[0])
                     except ValueError:
                        aln_len = "-"
                     if template not in templates:
                        templates[template] = aln_len
               elif l.startswith('*****'):
                  pass
               else:
                  # END TABLE
                  printOut("</table><br>\n")
                  in_table = False
            elif l.startswith('*****'):
               # Start Table
               in_table = True
               nohead = True
               if twoheaders:
                  upper_header = True
               else:
                  upper_header = False
               header1 = ""
               # printOut("<table class='center virresults' width='85%'>\n")
               printOut("<table class='%s' width='%s'>\n"%(css_classes, twidth))
            elif l.startswith('Extended Output:'):
               in_ext = True
               # Print extended output button
               printOut("<br><br>")
               printOut("<script type='text/javascript'>"
                        "function printOutput(tag){"
                        "var ele = document.getElementById(tag).style;"
                        "if (ele.display=='block'){ele.display='none';}"
                        "else{ele.display='block';}}</script>\n")
               printOut("<center><button type='button' "
                        "onclick='printOutput(&quot;eo&quot;)'>"
                        "extended output</button></center>"
                        "<div id='eo' class='hide'>")
               printOut("<pre><br>")
            elif ':' in l:
               var, val = l.split(':',1)
               if var == "Notes":
                  printOut("<h4>%s: %s</h4>"%(var.strip(), val.strip()))
               else:
                  printOut("<h2>%s: &nbsp;<i class='grey'>%s</i></h2>"%(var.strip(), val.strip()))
            #elif '-' in l:
            #   printOut("<h4>%s</h4>"%(l.strip()))
            elif in_note:
               if l.strip() == '':
                  # End and print note
                  in_note = False
                  printOut("<div class='red' style='width:%s'>%s</div>\n\n"%(twidth, ' '.join(note)))
               else:
                  note.append(l.strip())
            elif l.startswith('* '):
               # Start note
               in_note = True
               note = [l.strip()[2:]]

         if in_ext:
            # End extended output
            printOut("</pre><br><br></div>")

parser = ArgumentParser()
parser.add_argument("-r", "--resultsfile", dest="resultsfile", help="Results.txt file from HIVDRFinder")
parser.add_argument("-etc", "--etc", dest="etcfolder", help="Location of the etc folder")
args = parser.parse_args()

print('<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">')
print('<html lang="en">')
print('<head>')
print('<meta name="keywords" content="CGE Server">')
print('<meta http-equiv="content-type" content="text/html;charset=iso-8859-1">')
print('<title>Center for Genomic Epidemology - Results</title>')

print('<link href="'+str(args.etcfolder)+'/main.css" rel="StyleSheet" type="text/css" media="all">')
print('<link href="'+str(args.etcfolder)+'login.css" rel="StyleSheet" type="text/css" media="all">')
print('<link href="'+str(args.etcfolder)+'out_colors.css" rel="StyleSheet" type="text/css" media="all">')
print('</head>')
print("<body><div class='body'>")
print("<!-- START CONTENT -->")
print("<h1>HIVDRFinder-1.1 Server - Results</h1>")
vir_out3html(str(args.resultsfile), min_ab= 0.04, nuc_link =False, template_index=[0], css_classes = "center virresults", twoheaders=True)
